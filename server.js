/**
 * Implementation of web server.
 */

const path= require('path');

// First import the express library.
const express= require('express');

// Then create an app object.
const app= express();

// Now obtain the port in which the server will be run.
// If not available, just give 3000 as the default port.
const port= process.env.PORT || 3000;

app.use(function(request, response, next){
	
	if(request.headers['x-forwarded-proto'] === 'https')
	{
		console.log(request.headers.host + request.url);
		response.redirect(request.headers.host + request.url);
	}	
	else
	{
		next();
	}
});

// Now tell the app which folder to serve.
app.use(express.static('public'));

// This middleware is required when 'browserHistory' is used instead of 'hashHisory'.
// The below middleware will serve the index.html file for all unmatched routes like /about and so on.
app.get('*', function(request, response){
	response.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

// Now start the server at a particular port.
app.listen(port, function(){
	console.log('Express running in port '+port);
});