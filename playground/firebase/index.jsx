import firebase from 'firebase';

// Initialize Firebase
var config = {
	apiKey: "AIzaSyCxucyTmsH48VX2BTfzou4kK4qaG3Vg7oM",
	authDomain: "todo-app-a7f6e.firebaseapp.com",
	databaseURL: "https://todo-app-a7f6e.firebaseio.com",
	projectId: "todo-app-a7f6e",
	storageBucket: "todo-app-a7f6e.appspot.com",
	messagingSenderId: "294888508649"
};

firebase.initializeApp(config);

let firebaseRef= firebase.database().ref();

firebaseRef.set({
	app: {
		name: "Todo",
		version: "1.0"
	},
	isRunning: true,
	user: {
		name: "Jennish",
		age: 26
	}
});

// firebaseRef.child('app').once('value').then((snapshot) => {
// 	console.log('Got entire database ', snapshot.key, snapshot.val());
// },
// (error) => {
// 	console.log('Unable to fetch value '+error)
// });

firebaseRef.on('value', (snapshot) => {
	console.log('Data Fetched ', snapshot.key, snapshot.val());
});

let todosRef= firebaseRef.child('todos');

todosRef.on('child_added', (snapshot) => {
	console.log('Child added ', snapshot.key, snapshot.val());
});

let newTodosRef= todosRef.push({
	text: 'Go to Gym'
});

newTodosRef= todosRef.push({
	text: 'Call De'
});