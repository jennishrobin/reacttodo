// function add(a, b)
// {
// 	return a + b;
// }

// let toAdd= [9, 5];
// console.log(add(...toAdd));


// let groupA= ['Jennish', 'Jare'];
// let groupB= ['Vikram'];

// let final= [...groupA, 3, ...groupB];
// console.log(final);

let person1= ['Jennish', 26];
let person2= ['Jare', 27];

function greet(name, age)
{
	console.log('Hi '+name+', you are '+age);
}

greet(...person1);
greet(...person2);

var names= ['Mike', 'Ben'];
var final= ['Jennish'];

final= [...final, ...names];

final.forEach( function(name, index) 
{
	console.log('Hi '+name);
});