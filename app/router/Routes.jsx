import React from 'react';
import {Router, browserHistory} from 'react-router';

import Login from 'Login';
import firebase from 'Database';

const routesConfig= {
	path: '/',
	indexRoute: {
		component: Login, 
		onEnter(nextState, replace, next) {
			console.log('Inside Index');
			if(firebase.auth().currentUser)
			{
				console.log('User exist');
				replace('/todos');
			}
			console.log('User not found');
			next();
		}
	},
	childRoutes: [
		{
			path: 'todos',
			getComponent(location, cb) {
				System.import('TodoApp').then((module) => cb(null, module.default) );
			},
			onEnter(nextState, replace, next) {
				console.log('Inside Todos');
				if(!firebase.auth().currentUser)
				{
					console.log('User not found');
					replace('/');
				}
				console.log('User Found');
				next();
			}
		}
	]
};

const Routes= () => <Router history={browserHistory} routes={routesConfig} />;
export default Routes;