import React from 'react';
import {connect} from 'react-redux';
import moment from 'moment';

import * as actions from 'Actions';

export class Todo extends React.Component {

	constructor(props) {
		super(props);
		this.updateTodo= this.updateTodo.bind(this);	
	}

	render()
	{
		let {id, text, completed, createdAt, completedAt, dispatch, edit, edited, editedAt}= this.props;
		let todoClassName= completed ? 'todo todo-completed' : 'todo';
		console.log(id+' rendered');
		if(edit=== true)
		{
			return (
				<label className={todoClassName}>
					<div>
						<input type="text" defaultValue={text} ref="todo" />
						{this.renderTodoStatus(edited, completed, editedAt, completedAt, createdAt)}
					</div>
					<div className="todo__right">
						<button className="button tiny custom-button" onClick= {this.updateTodo}>Save</button>
						<button className="alert button tiny custom-button" onClick= {
							(e) => {
								e.preventDefault(); 
								dispatch(actions.cancelEdit(id));
							} 
						}>Cancel</button>
					</div>
				</label>
			);
		}
		else
		{
			return (
				<label className={todoClassName}>
					<div>
						<input type="checkbox" checked={completed} onChange={() => {dispatch(actions.startToggleTodo(id, !completed))} } />
					</div>
					<div>
						<p>{text}</p>
						{this.renderTodoStatus(edited, completed, editedAt, completedAt, createdAt)}
					</div>
					<div className="todo__right">
						<button className="button tiny custom-button" onClick= {
							(e) => {
								e.preventDefault();
								dispatch(actions.editTodo(id));
							} 
						}>Edit</button>
						<button className="alert button tiny custom-button" onClick={
							(e) => {
								e.preventDefault();
								dispatch(actions.startDelete(id));
							} 
						}>Delete</button>
					</div>
				</label>
			);
		}
	}

	renderTodoStatus(edited, completed, editedAt, completedAt, createdAt)
	{
		let message= '';
		let time= undefined;

		if(completed=== true)
		{
			message= 'Completed';
			time= completedAt;
		}
		else if(edited=== true)
		{
			message= 'Edited';
			time= editedAt;	
		}
		else
		{
			message= 'Created';
			time= createdAt;
		}

		return <p className="todo__subtext">{this.renderDate(message, time)}</p>
	}

	renderDate(message, timestamp) {
		return message +' '+ moment.unix(timestamp).format('MMM Do YYYY @ h:mm a');
	}

	updateTodo(e)
	{
		e.preventDefault();
		
		let todo= this.refs.todo.value;
		let {dispatch, id}= this.props;

		if(typeof todo!== 'undefined' && todo.length > 0)
		{
			dispatch(actions.saveUpdate(id, todo));
		}
	}
};

export default connect()(Todo);