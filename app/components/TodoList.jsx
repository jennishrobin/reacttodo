import React from 'react';
import {connect} from 'react-redux';
import TodoAPI from 'TodoAPI'; 

import Todo from 'Todo';

export class TodoList extends React.Component {

	renderTodoList()
	{
		let {todos, showCompleted, searchText}= this.props;
		let filteredTodos= TodoAPI.filterTodos(todos, showCompleted, searchText);

		if(filteredTodos.length=== 0)
		{
			return <p className="container__message">Nothing To Do</p>;
		}

		return filteredTodos.map((todo) => {
			return (
				<Todo key={todo.id} {...todo} />
			);
		});
	}

	render()
	{
		console.log('TodoList Rendered');
		return (
			<div>
				{this.renderTodoList()}
			</div>
		);
	}
};

export default connect( (state) => {
	return state;
})(TodoList);