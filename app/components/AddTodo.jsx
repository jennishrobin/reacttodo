import React from 'react';
import {connect} from 'react-redux';

import {startAddTodo} from 'Actions';

export class AddTodo extends React.Component {

	constructor(props) {
		super(props);
		this.handleSubmit= this.handleSubmit.bind(this);		
	}

	handleSubmit(e)
	{
		e.preventDefault();
		let text= this.refs.text.value;
		
		if(typeof text!== 'undefined' && text.length > 0)
		{
			this.refs.text.value= '';
			this.props.dispatch(startAddTodo(text));
		}
		else
		{
			this.refs.text.focus();
		}
	}

	render ()
	{
		return (
			<div className="container__footer">
				<form ref="todo_add_form" onSubmit= {this.handleSubmit}>
					<input type="text" ref="text" placeholder="What do you need to do" />
					<button className="button expanded">Add</button>
				</form>
			</div>
		);
	}

};

export default connect()(AddTodo);