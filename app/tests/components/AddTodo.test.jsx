import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import jQuery from 'jquery';

import {AddTodo} from 'AddTodo';
import * as actions from 'Actions';

describe('AddTodo', () => {

	it('should exist', () => {
		expect(AddTodo).toExist();
	});

	it('should dispatch ADD_TODO when a valid text has been entered', () => {

		let todo_text= 'Check mail';
		let action= actions.startAddTodo(todo_text);

		let spy= expect.createSpy();
		let addtodo= TestUtils.renderIntoDocument(<AddTodo dispatch={spy} />);

		addtodo.refs.text.value= todo_text;
		TestUtils.Simulate.submit(addtodo.refs.todo_add_form);
		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should not dispatch ADD_TODO when invalid text has been entered', () => {

		let todo_text= '';
		let spy= expect.createSpy();
		let addtodo= TestUtils.renderIntoDocument(<AddTodo dispatch={spy} />);

		addtodo.refs.text.value= todo_text;
		TestUtils.Simulate.submit(addtodo.refs.todo_add_form);
		expect(spy).toNotHaveBeenCalled();
	});
});