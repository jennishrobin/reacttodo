import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import jQuery from 'jquery';

import {TodoSearch} from 'TodoSearch';

describe('TodoSearch', () => {

	it('should exist', () => {
		expect(TodoSearch).toExist();
	});

	it('should dispatch SET_SEARCH_TEXT on input change', () => {
		let searchText= 'Dog';
		let spy= expect.createSpy();
		let todosearch= TestUtils.renderIntoDocument(<TodoSearch dispatch={spy} />);

		let action= {
			type: "SET_SEARCH_TEXT",
			searchText
		};

		todosearch.refs.searchText.value= searchText;
		TestUtils.Simulate.change(todosearch.refs.searchText);

		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should dispatch TOGGLE_SHOW_COMPLETED when checkbox checked', () => {

		let spy= expect.createSpy();
		let todosearch= TestUtils.renderIntoDocument(<TodoSearch dispatch={spy} />);

		let action= {
			type: "TOGGLE_SHOW_COMPLETED"
		};

		todosearch.refs.showCompleted.checked= true;
		TestUtils.Simulate.change(todosearch.refs.showCompleted);

		expect(spy).toHaveBeenCalledWith(action);
	});
});