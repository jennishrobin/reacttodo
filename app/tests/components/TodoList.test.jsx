import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';

import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import jQuery from 'jquery';

import ConnectedTodoList, {TodoList} from 'TodoList';
import connectedTodo, {Todo} from 'Todo';
import configure from 'configureStore';

describe('TodoList', () => {

	it('should exist', () => {
		expect(TodoList).toExist();
	});

	it('should render 1 Todo Component for each Todo item', () => {
		let todos= [
			{
				id: 1,
				text: 'Do Something',
				completed: false,
				edit: false,
				edited: false,
				editedAt: undefined,
				completedAt: undefined,
				createdAt: 500
			},
			{
				id: 2,
				text: 'Book Guardians of Galaxy',
				completed: false,
				edit: false,
				edited: false,
				editedAt: undefined,
				completedAt: undefined,
				createdAt: 500
			}
		];

		let store= configure({
			todos
		});

		let provider= TestUtils.renderIntoDocument(
			<Provider store={store}>
				<ConnectedTodoList />
			</Provider>
		);
		
		// Since data has to be passed from store to TodoList, we have to use ConnectedTodoList
		let todoList= TestUtils.scryRenderedComponentsWithType(provider, TodoList)[0];

		// However, no data will be accessed from store by Todo component, we can pass the pure component.
		// In Todo, the data will be passed by TodoList as props.
		let todosComponents= TestUtils.scryRenderedComponentsWithType(todoList, Todo);

		expect(todosComponents.length).toBe(todos.length);
	});

	it('should render empty message if no todos', () => {
		let todos= [];

		let todolist= TestUtils.renderIntoDocument(<TodoList todos={todos} showCompleted= {false} searchText={''} />);
		let $el= $(ReactDOM.findDOMNode(todolist));

		expect($el.find('.container__message').length).toBe(1);
	});
});
