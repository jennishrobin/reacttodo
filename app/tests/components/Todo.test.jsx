import React from 'react';
import ReactDOM from 'react-dom';
import expect from 'expect';
import TestUtils from 'react-addons-test-utils';
import jQuery from 'jquery';
import moment from 'moment';

import {Todo} from 'Todo';
import * as actions from 'Actions';

describe('Todo', () => {

	it('should exist', () => {
		expect(Todo).toExist();
	});

	it('should dispatch UPDATE_TODO action on changing the status of the todo.', () => {
		let spy= expect.createSpy();

		let todos= {
				id: 199,
				text: 'Write todo.test.jsx',
				completed: false,
				edit: false,
				edited: false,
				createdAt: 33,
				editedAt: undefined,
				completedAt: undefined
			};

		let action= actions.startToggleTodo(todos.id, !todos.completed)

		let todo= TestUtils.renderIntoDocument(<Todo {...todos} dispatch={spy} />);
		let $el= $(ReactDOM.findDOMNode(todo));
		TestUtils.Simulate.change($el.find('input')[0]);

		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should dispatch the EDIT_TODO action on clicking edit button.', () => {
		let spy= expect.createSpy();

		let todos= {
				id: 199,
				text: 'Write todo.test.jsx',
				completed: false,
				edit: false,
				edited: false,
				createdAt: 33,
				editedAt: undefined,
				completedAt: undefined
			};

		let todo= TestUtils.renderIntoDocument(<Todo {...todos} dispatch={spy} />);
		let $el= $(ReactDOM.findDOMNode(todo));

		TestUtils.Simulate.click($el.find('.custom-button')[0]);
		expect(spy).toHaveBeenCalledWith({
			type: "EDIT_TODO",
			id: 199
		});
	});

	it('should trigger the delete operation on clicking delete button.', () => {
		let spy= expect.createSpy();

		let todos= {
				id: 199,
				text: 'Write todo.test.jsx',
				completed: false,
				edit: false,
				edited: false,
				createdAt: 33,
				editedAt: undefined,
				completedAt: undefined
			};

		let action= actions.startDelete(todos.id);

		let todo= TestUtils.renderIntoDocument(<Todo {...todos} dispatch={spy} />);
		let $el= $(ReactDOM.findDOMNode(todo));

		TestUtils.Simulate.click($el.find('.custom-button')[1]);
		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should trigger the saving process on clicking save button.', () => {
		let spy= expect.createSpy();

		let todos= {
			id: 199,
			text: 'Write todo.test.jsx',
			completed: false,
			edit: true,
			edited: false,
			createdAt: 33,
			editedAt: undefined,
			completedAt: undefined
		};

		let todo= TestUtils.renderIntoDocument(<Todo {...todos} dispatch={spy} />);
		let $el= $(ReactDOM.findDOMNode(todo));
		todo.refs.todo.value= "New text";

		let action= actions.saveUpdate(todos.id, todo.refs.todo.value); 

		TestUtils.Simulate.click($el.find('.custom-button')[0]);
		expect(spy).toHaveBeenCalledWith(action);
	});

	it('should dispatch CANCEL_EDIT action on clicking the cancel button', () => {
		let spy= expect.createSpy();

		let todos= {
			id: 199,
			text: 'Write todo.test.jsx',
			completed: false,
			edit: true,
			edited: false,
			createdAt: 33,
			editedAt: undefined,
			completedAt: undefined
		};

		let todo= TestUtils.renderIntoDocument(<Todo {...todos} dispatch={spy} />);
		let $el= $(ReactDOM.findDOMNode(todo));

		TestUtils.Simulate.click($el.find('.custom-button')[1]);
		expect(spy).toHaveBeenCalledWith({
			type: "CANCEL_EDIT",
			id: 199
		});
	});
});