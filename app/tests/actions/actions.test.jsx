import expect from 'expect';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import firebase, {firebaseRef} from 'Database';
import * as actions from 'Actions';

let createMockStore= configureMockStore([thunk]); 

describe('Actions', () => {
	it('should generate search text action', () => {
		let action= {
			type: "SET_SEARCH_TEXT",
			searchText: "Some search text"
		};

		let res= actions.setSearchText(action.searchText);
		expect(res).toEqual(action);
	});

	it('should generate addtodo action', () => {
		let action= {
			type: "ADD_TODO",
			todo: {
				id: 'abc123',
				text: 'Something to do',
				completed: false,
				edit: false,
				edited: false,
				createdAt: 1244,
				editedAt: null,
				completedAt: null
			}
		};

		let res= actions.addTodo(action.todo);
		expect(res).toEqual(action);
	});

	it('should generate addtodos action', () => {

		let todos= [
			{
				id: 111,
				text: "Walk the dog.",
				completed: false,
				completedAt: undefined,
				createdAt: 33000
			}
		];

		let action= {
			type: "ADD_TODOS",
			todos 
		};

		let res= actions.addTodos(todos);
		expect(res).toEqual(action);
	});

	it('should generate toggle show completed action', () => {
		let action= {
			type: "TOGGLE_SHOW_COMPLETED"
		};

		let res= actions.toggleShowCompleted();
		expect(res).toEqual(action);
	});

	it('should generate edit todo action', () => {
		let action= {
			type : "EDIT_TODO",
			id:99
		};

		let resp= actions.editTodo(action.id);
		expect(resp).toEqual(action);
	});

	it('should generate update todo action', () => {
		let action= {
			type: "UPDATE_TODO",
			id: 99,
			updates: {
				text: "New text",
				completed: true,
				completedAt: 33000
			}
		};

		let resp= actions.updateTodo(action.id, action.updates);
		expect(resp).toEqual(action);
	});

	it('should generate cancel edit action', () => {
		let action= {
			type: "CANCEL_EDIT",
			id: 99
		};

		let resp= actions.cancelEdit(action.id);
		expect(resp).toEqual(action);
	});

	it('should generate delete todo action', () => {
		let action= {
			type: "DELETE_TODO",
			id: 99
		};

		let resp= actions.deleteTodo(action.id);
		expect(resp).toEqual(action);
	});

	describe('Tests with firebase todos', () => {
		let testTodoRef;
		let user_id;
		let todosRef;

		beforeEach((done) => {

			firebase.auth().signInAnonymously()
				.then((user) => {
					user_id= user.uid;
					todosRef= firebaseRef.child(`users/${user_id}/todos`);

					return todosRef.remove();
				})
				.then( () => {
					testTodoRef= todosRef.push();
					return testTodoRef.set({
						text: 'Something to do',
						completed: false,
						createdAt: 12345,
						edit: false,
					});
				})
				.then(() => done())
				.catch(done);
		});

		afterEach((done) => {
			todosRef.remove().then(() => done());
		});

		it('should toggle todo and dispatch ADD_TODOS action', (done) => {
			const store= createMockStore({auth: {user_id} });

			let action= actions.startToggleTodo(testTodoRef.key, true);

			store.dispatch(action)
				.then(() => {
					let mockActions= store.getActions();

					expect(mockActions[0]).toEqual('ADD_TODOS');
					expect(mockActions[0].todos[0].completed).toEqual(true);
					expect(mockActions[0].todos[0].completedAt).toExist();
				})
				.catch(done);
		});

		it('should populate todos and dispatch ADD_TODOS', () => {
			const store= createMockStore({auth: {user_id} });
			let action= actions.startAddTodos();
			
			store.dispatch(action);
			let mockActions= store.getActions();

			expect(mockActions[0].type).toEqual('ADD_TODOS');
			expect(mockActions[0].todos.length).toBe(1);
			expect(mockActions[0].todos[0].text).toEqual('Something to do');
		});

		it('should create todo and dispatch add todo', (done) => {
			const store= createMockStore({auth: {user_id} });
			const todoText= 'My todo item';

			store.dispatch(actions.startAddTodo(todoText)).then( () => {
				let mockActions= store.getActions();

				expect(mockActions[0]).toInclude({
					type: "ADD_TODO"
				});

				expect(mockActions[0].todo).toInclude({
					text: todoText
				});

				done();

			}).catch(done)
		});

		it('should save the todo changes and dispatch ADD_TODOS', () => {
			const store= createMockStore({auth: {user_id} });
			const todoText= 'Todo Changed';

			let action= actions.saveUpdate(testTodoRef.key, todoText);

			// Since we are not dispatching any actions after the changes has been saved.
			// I am not sure what needs to be tested.
			let mockActions= store.getActions();

			expect(mockActions[0].type).toEqual('ADD_TODOS');
			expect(mockActions[0].todos[0].text).toEqual(todoText);
		});

		it('should delete a delete and dispatch ADD_TODOS', () => {
			const store= createMockStore({auth: {user_id} });

			let action= actions.startDelete(testTodoRef.key);
			let mockActions= store.getActions();

			expect(mockActions[0].type).toEqual('ADD_TODOS');
			expect(mockActions[0].todos.length).toEqual(0);
		});
	});

	describe('Authentication Tests', () => {

		it('should generate TOGGLE_AUTH_CHECK_STATUS action', () => {
			let action= {
				type: "TOGGLE_AUTH_CHECK_STATUS"
			};

			let resp= actions.authCheckCompleted();
			expect(resp).toEqual(action);
		});

		it('should generate login action', () => {
			let action= {
				type: 'LOGIN',
				user_id: '123sz'
			};

			let resp= actions.login('123sz');
			expect(resp).toEqual(action);
		});

		it('should generate logout action', () => {
			let action= {
				type: "LOGOUT"
			};

			let resp= actions.logout();
			expect(resp).toEqual(action);
		});
	});
});