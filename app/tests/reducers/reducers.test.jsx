import expect from 'expect';
import df from 'deep-freeze-strict';

import * as reducers from 'Reducers';

describe('Reducers', () => {
	describe('Search Text', () => {
		it('should set search text', () => {
			let action= {
				type: "SET_SEARCH_TEXT",
				searchText: "Dog"
			};

			let resp= reducers.searchTextReducer(df(''), df(action));
			expect(resp).toEqual(action.searchText);
		});
	});

	describe('Show Completed', () => {
		it('should flip the show completed status', () => {
			let action= {
				type: "TOGGLE_SHOW_COMPLETED",
			};

			let resp= reducers.showCompletedReducer(df(false), df(action));
			expect(resp).toEqual(true);
		});
	});

	describe('Todos Reducer', () => {
		it('should add new todo', () => {
			let action= {
				type: "ADD_TODO",
				todo: {
					id: 'abc123',
					text: 'Something to do',
					completed: false,
					createdAt: 123,
					edit: false,
					edited: false,
					editedAt: null,
					completedAt: null
				}
			};

			let resp= reducers.todosReducer(df([]), df(action));

			expect(resp.length).toEqual(1);
			expect(resp[0]).toEqual(action.todo);
		});

		it('should flip the completed status of the todo and also update the completedAt time', () => {
			let todos= [
				{
					id: 1,
					text: 'Book flight ticket',
					completed: true,
					edit: false,
					edited: false,
					editedAt: 124,
					createdAt: 123,
					completedAt: 125
				}
			];

			let action= {
				type: "UPDATE_TODO",
				id: 1,
				updates: {
					completed: false,
					completedAt: null
				}
			};

			let resp= reducers.todosReducer(df(todos), df(action));

			expect(resp[0].completed).toEqual(false);
			expect(resp[0].completedAt).toEqual(action.updates.completedAt);
			expect(resp[0].text).toEqual(todos[0].text);
		});

		it('should add existing todos', () => {
			let todos= [
				{
					id: 111,
					text: "Walk the dog.",
					completed: false,
					edit: false,
					edited: false,
					editedAt: undefined,
					completedAt: undefined,
					createdAt: 33000
				}
			];

			let action= {
				type: "ADD_TODOS",
				todos 
			};

			let resp= reducers.todosReducer(df([]), df(action));
			expect(resp.length).toEqual(1);
			expect(resp[0]).toEqual(todos[0]);
		});

		it('should wipeout todos on logout', () => {
			let action= {
				type: 'LOGOUT'
			};

			let todos= [
				{
					id: 111,
					text: "Walk the dog.",
					completed: false,
					edit: false,
					edited: false,
					editedAt: undefined,
					completedAt: undefined,
					createdAt: 33000
				}
			];

			let resp= reducers.todosReducer(df(todos), df(action));
			expect(resp.length).toBe(0);
		});

		it('should flip the edit status of the todo to true', () => {
			let todos= [
				{
					id: 111,
					text: "Walk the dog.",
					completed: false,
					edit: false,
					edited: false,
					editedAt: undefined,
					completedAt: undefined,
					createdAt: 33000
				}
			];

			let action= {
				type: "EDIT_TODO",
				id: 111
			};

			let resp= reducers.todosReducer(df(todos), df(action));
			expect(resp[0].edit).toEqual(true);
		});

		it('should flip the edit status of the todo to false if the edit has been cancelled.', () => {
			let todos= [
				{
					id: 111,
					text: "Walk the dog.",
					completed: false,
					edit: true,
					edited: false,
					editedAt: undefined,
					completedAt: undefined,
					createdAt: 33000
				}
			];

			let action= {
				type: "CANCEL_EDIT",
				id: 111
			};

			let resp= reducers.todosReducer(df(todos), df(action));
			expect(resp[0].edit).toEqual(false);
		});

		it('should update the todo', () => {
			let todos= [
				{
					id: 1,
					text: 'Go to gym',
					edit: false,
					completed: false,
					completedAt: undefined,
					edited: false,
					editedAt: undefined,
					createdAt: 33000
				}
			];

			let action= {
				type: "UPDATE_TODO",
				id: 1,
				updates: {
					text: 'Go for a movie',
					edited: true,
					editedAt: 123
				}
			}; 

			let resp= reducers.todosReducer(df(todos), df(action));
			expect(resp[0].edited).toEqual(true);
			expect(resp[0].editedAt).toBeA('number');
			expect(resp[0].text).toEqual(action.updates.text);
		});

		it('should delete a todo', () => {

			let todos= [
				{
					id: 1,
					text: 'Go to gym',
					edit: false,
					completed: false,
					completedAt: undefined,
					edited: false,
					editedAt: undefined,
					createdAt: 33000
				}
			];

			let action= {
				type: "DELETE_TODO",
				id:1
			};

			let resp= reducers.todosReducer(df(todos), df(action));
			expect(resp.length).toEqual(0);
		});
	});
	
	describe('Auth Status Check Reducer', () => {

		it('should toggle the value of authStatusObtained to TRUE', () => {
			let action= {
				type: 'TOGGLE_AUTH_CHECK_STATUS'
			};

			let resp= reducers.authStatusCheckReducer(false, action);
			expect(resp).toBe(true);
		});
	});

	describe('Auth Reducer', () => {

		it('should store user id on log-in', () => {
			let action= {
				type: "LOGIN",
				user_id: 123
			};

			let resp= reducers.authReducer(df({}), df(action));
			expect(resp.user_id).toEqual(action.user_id);
		});

		it('should remove user id on logout', () => {

			let auth= {
				user_id: 123
			};

			let action= {
				type: "LOGOUT"
			};

			let resp= reducers.authReducer(df(auth), df(action));
			expect(resp.user_id).toNotExist();
			expect(resp).toEqual({});
		});
	});
});