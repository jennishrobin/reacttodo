import firebase, {firebaseRef} from 'Database';
import moment from 'moment';

import {githubProvider} from 'Database';

export let setSearchText= (searchText) => {
	return {
		type: "SET_SEARCH_TEXT",
		searchText
	};
};

export let addTodo= (todo) => {
	return {
		type: "ADD_TODO",
		todo
	};
};

export let startAddTodo= (text) => {
	return (dispatch, getState) => {
		let todo= {
					text,
					completed: false,
					edited: false,
					edit: false,
					createdAt: moment().unix(),
					editedAt: null,
					completedAt: null
				};

		let user_id= getState().auth.user_id;
		let todoRef= firebaseRef.child(`users/${user_id}/todos`).push(todo);

		return todoRef.then(() => {
			/*dispatch(addTodo({
				...todo,
				id: todoRef.key
			}));*/
		});
	};
};

export let startAddTodos= () => {
	return (dispatch, getState) => {

		let user_id= getState().auth.user_id;
		let todosRef= firebaseRef.child(`users/${user_id}/todos`);

		return todosRef.on('value', (snapshot) => {
			let todos= snapshot.val() || {};
			let parsedTodos= [];

			Object.keys(todos).forEach( (todoId) => {
				parsedTodos.push({
					id: todoId,
					...todos[todoId]
				});
			});

			dispatch(addTodos(parsedTodos));
		});
	};
};

export let addTodos= (todos) => {
	return {
		type: "ADD_TODOS",
		todos
	};
};

export let toggleShowCompleted= () => {
	return {
		type: "TOGGLE_SHOW_COMPLETED"
	};
};

export let startToggleTodo= (id, completed) => {
	return (dispatch, getState) => {

		let user_id= getState().auth.user_id;
		let todoRef= firebaseRef.child(`users/${user_id}/todos/${id}`);

		let updates= {
			completed,
			completedAt: completed ?  moment().unix() : null
		};

		return todoRef.update(updates).then(() => {
			// dispatch(updateTodo(id, updates));
		});
	}; 
};

export let editTodo= (id) => {
	return {
		type: "EDIT_TODO",
		id
	};
};


export let saveUpdate= (id, text) => {
	return (dispatch, getState) => {
		let user_id= getState().auth.user_id;
		let todoRef= firebaseRef.child(`users/${user_id}/todos/${id}`);

		let updates= {
			text,
			edit: false,
			edited: true,
			editedAt: moment().unix()
		};

		return todoRef.update(updates)
			.then(() => {
				// ADD_TODOS will get dispatched once the update has been successfully saved.
			})
			.catch((error) => {
				console.log('Update Failed: '+error.message);
			});
	};
};

export let updateTodo= (id, updates) => {
	return {
		type: "UPDATE_TODO",
		id,
		updates
	};
};

export let startDelete= (id) => {
	return (dispatch, getState) => {
		let user_id= getState().auth.user_id;
		let todoRef= firebaseRef.child(`users/${user_id}/todos/${id}`);

		return todoRef.remove()
			.then(() => {
				// ADD_TODOS will get dispatched when the todo has been removed from database.
			})
			.catch((error) => {
				console.log('Remove Failed: '+ error.message);
			});
	};
};

export let deleteTodo= (id) => {
	return {
		type: "DELETE_TODO",
		id
	};
};

export let cancelEdit= (id) => {
	return {
		type: "CANCEL_EDIT",
		id
	};
};

// Action which initiates the log-in process.
export let startLogin= () => {
	return (dispatch, getState) => {
		return firebase.auth().signInWithPopup(githubProvider).then((result) => {
			console.log('Auth Worked', result.user.uid);
		},
		(error) => {
			console.log('Log in failed.', error);
		});
	}
};

// Action which initiates the logout process.
export let startLogout= () => {
	return (dispatch, getState) => {
		return firebase.auth().signOut().then(() => {
			console.log('Logged Out');
		},
		() => {
			console.log('Sign out failed.');
		})
	}
};

// Action to add user id to store, once logged in
export let login= (user_id) => {
	return {
		type: "LOGIN",
		user_id
	};
};

// Action to remove user id from store, once logged out
export let logout= () => {
	return {
		type: "LOGOUT"
	};
};

export let authCheckCompleted= () => {
	console.log('Action dispatched');
	return {
		type: 'TOGGLE_AUTH_CHECK_STATUS'
	};
};