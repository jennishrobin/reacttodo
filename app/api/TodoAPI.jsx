export default {
	filterTodos: function(todos, showCompleted, searchText)
	{
		let filteredTodos= todos;

		searchText= searchText.toLowerCase();

		// Filter by showCompleted
		if(showCompleted=== false)
		{
			filteredTodos= filteredTodos.filter(todo => !todo.completed);
		}

		// Filter by searchText
		if(searchText.length > 0)
		{
			filteredTodos= filteredTodos.filter(todo => {
				return todo.text.toLowerCase().indexOf(searchText)!== -1 ? true : false;	
			});
		}
		
		// Sort todos with non completed first.
		filteredTodos.sort((a, b) => {
			if(a.completed=== false && b.completed=== true)
			{
				return -1;
			}
			else if(a.completed=== true && b.completed=== false)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}); 

		return filteredTodos;
	}
};