import * as redux from 'redux';
import thunk from 'redux-thunk';

import * as reducers from 'Reducers';

let configure= (initialState= {}) => {
	let reducer= redux.combineReducers({
		searchText 			: reducers.searchTextReducer,
		showCompleted 		: reducers.showCompletedReducer,
		todos 				: reducers.todosReducer,
		authStatusObtained 	: reducers.authStatusCheckReducer,
		auth 				: reducers.authReducer
	});

	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || redux.compose;

	let store= redux.createStore(reducer, initialState, composeEnhancers(
		redux.applyMiddleware(thunk)
	));

	return store;
};

export default configure;