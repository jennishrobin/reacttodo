import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {browserHistory} from 'react-router';

// import jQuery from 'script-loader!jquery';
// import 'script-loader!Foundation';

import 'applicationStyles';
import Routes from 'Routes';
import {authCheckCompleted, startAddTodos, login, logout} from 'Actions';
import configure from 'configureStore';
import TodoAPI from 'TodoAPI';
import firebase from 'Database';

let store= configure();

// A flag which denotes whether
let rendered= false;

firebase.auth().onAuthStateChanged((user) => {

	console.log('Status change callback.');

	// Check whether the DOM has already been rendered.
	// If not, then dispatch the action which will update the status of auth check, which in turn will
	// trigger the subscribe callback and then render the DOM.
	rendered=== false ? store.dispatch(authCheckCompleted()) : undefined;

	// Check whether the 'user' data is available.
	if(user)
	{
		// Dispatch action to store the user id in store.
		store.dispatch(login(user.uid));

		// Dispatch action which will fetch all the todos from firebase and add it in our store.
		// This must be done once the user has logged in.
		store.dispatch(startAddTodos());
		browserHistory.push('/todos');
	}
	else
	{
		// Dispatch action to remove the user id from store.
		store.dispatch(logout());
		browserHistory.push('/');
	}
});

store.subscribe(() => {

	// If the DOM has already been rendered, then no need to dispatch any action.
	if(rendered=== false)
	{
		let state= store.getState();

		if(state.authStatusObtained=== true)
		{
			console.log('DOM rendered');
			ReactDOM.render(<Provider store={store}><Routes /></Provider>, document.getElementById('app'));
			rendered= true;
		}
		else
		{
			ReactDOM.render(<h1>Loading...</h1>, document.getElementById('app'));
		}
	}
});

$(document).ready(function() {
	$(document).foundation();
});