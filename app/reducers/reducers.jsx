import uuid from 'node-uuid';
import moment from 'moment';

export let searchTextReducer= (state = '', action) => {
	switch(action.type)
	{
		case 'SET_SEARCH_TEXT':
			return action.searchText;

		default:
			return state;
	}
};


export let showCompletedReducer= (state= false, action) => {

	switch(action.type)
	{
		case 'TOGGLE_SHOW_COMPLETED':
			return !state;

		default:
			return state;
	}
};

export let todosReducer= (state= [], action) => {
	switch(action.type)
	{
		case 'ADD_TODO':
			return [
				...state,
				action.todo
			];

		case 'EDIT_TODO':
			return state.map((todo) => {
				if(todo.id === action.id)
				{
					return {
						...todo,
						edit: true
					};
				}
				return todo;
			});

		case 'CANCEL_EDIT':
			return state.map((todo) => {
				if(todo.id === action.id)
				{
					return {
						...todo,
						edit: false
					};
				}
				return todo;
			});

		case 'UPDATE_TODO':
			return state.map((todo) => {
				if(todo.id===action.id)
				{
					return {
						...todo,
						...action.updates
					};
				}
				return todo;
			});

		case 'DELETE_TODO':
			return state.filter((todo) => {
				if(todo.id=== action.id)
					return false;

				return true;
			});

		case 'TOGGLE_TODO':

			return state.map((todo) => {
				if(todo.id === action.id)
				{
					let newStatus= !todo.completed;
					return {
						...todo,
						completed: newStatus,
						completedAt: newStatus=== true ? moment().unix() : undefined
					};
				}
				return todo;
			});

		case 'ADD_TODOS':

			let todos= action.todos;
			if(todos.length > 0)
			{
				todos= todos.map((todo) => {
					return {
						...todo,
						edit: false
					};
				});
			}

			return [
				...todos
			];

		case 'LOGOUT':
			return [];

		default:
			return state;
	}
};

export let authStatusCheckReducer= (state=false, action) => {
	switch(action.type)
	{
		case 'TOGGLE_AUTH_CHECK_STATUS':
			return true;

		default:
			return state;
	}
};

// Auth reducers which will handle operation like 
// 1. Storing user id in store after user log in 
// 2. Removing user id from store after user logout.
export let authReducer= (state= {}, action) => {
	switch(action.type)
	{
		case 'LOGIN':
			return {
				...state,
				user_id: action.user_id
			};
			break;

		case 'LOGOUT':
			return {};
			break;

		default:
			return state;
	}
};