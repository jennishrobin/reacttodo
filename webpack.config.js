const webpack= require('webpack');
const path= require('path');
const ExtractTextPlugin= require('extract-text-webpack-plugin');
const HtmlWebpackPlugin= require('html-webpack-plugin');
const envFile= require('node-env-file');

const VENDOR= ['react', 'react-dom', 'react-router', 'Foundation', 'jquery', 'expect', 'moment', 'node-uuid', 'redux', 'react-redux', 'firebase', 'redux-mock-store', 'node-env-file'];

process.env.NODE_ENV= process.env.NODE_ENV || 'development';

try 
{
	envFile(path.resolve(__dirname, 'config/'+ process.env.NODE_ENV + '.env'));
} 
catch(e) 
{
	// statements
	console.log(e);
}

module.exports= {
	entry: {
		bundle: "./app/app.jsx",
		vendor: VENDOR
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: "[name].[chunkhash].js"
	},
	node: {
		fs: 'empty'
	},
	resolve: {
		modules: [path.resolve(__dirname, 'node_modules'), path.resolve(__dirname, 'app')],
		alias: {
			Login 					: "components/Login.jsx",
			TodoApp 				: "components/TodoApp.jsx",
			TodoList				: "components/TodoList.jsx",
			Todo 					: "components/Todo.jsx",
			AddTodo					: "components/AddTodo.jsx",
			TodoSearch				: "components/TodoSearch.jsx",
			Routes 					: "router/Routes.jsx",
			TodoAPI					: "api/TodoAPI.jsx",
			Actions 				: "actions/actions.jsx",
			Reducers 				: "reducers/reducers.jsx",
			configureStore 			: "store/configureStore.jsx",
			Database				: "firebase/index.js",
			Foundation 				: "foundation-sites/dist/js/foundation.min.js",
			jquery 					: "jquery/dist/jquery.min.js",
			applicationStyles 		: "styles/app.scss"
		}
	},
	module: {
		rules: [
			{
				use: "babel-loader",
				test: /\.jsx?$/,
				exclude: /node_modules/
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: [
						{
							loader: "css-loader"
						},
						{
							loader: "sass-loader",
							options: {
								includePaths: [
									path.resolve(__dirname, './node_modules/foundation-sites/scss')
								]
							}
						}
					]
				})
			}
		]
	},
	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: ["vendor", "manifest"]
		}),
		new HtmlWebpackPlugin({
			template: './app/index.html'
		}),
		new ExtractTextPlugin('style.css'),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new webpack.optimize.UglifyJsPlugin({
			compresser: {
				warnings: false
			}
		}),
		new webpack.DefinePlugin({
			'process.env' : {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV),
				API_KEY: JSON.stringify(process.env.API_KEY),
				AUTH_DOMAIN: JSON.stringify(process.env.AUTH_DOMAIN),
				DATABASE_URL: JSON.stringify(process.env.DATABASE_URL),
				PROJECT_ID: JSON.stringify(process.env.PROJECT_ID),
				STORAGE_BUCKET: JSON.stringify(process.env.STORAGE_BUCKET),
				MESSAGING_SENDER_ID: JSON.stringify(process.env.MESSAGING_SENDER_ID)
			}
		})
	],
	devtool: process.env.NODE_ENV=== 'production' ? undefined  : 'eval-source-map'
};